const got  = require('./data/data');

function nameWithS(got) {
    //4. Write a function called `nameWithS` which returns a array of names of all the people in `got` variable whose name includes `s` or `S`.
    // your code goes here
    
    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let namesWithS = (got.houses).map((house) => 
            (house.people).map((person) => 
                (person.name)).filter((name) => 
                    name.toLowerCase().includes('s')
                )
            )        
        .flat();
    
        return namesWithS;
    } else {
        return ;
    }     
}

module.exports = nameWithS;
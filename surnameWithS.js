const got = require('./data/data');

function surnameWithS(got) {
    //6. Write a function called `surnameWithS` which returns a array of names of all the people in `got` variable whoes surname is starting with `S`(capital s).
    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let surnamesWithS = (got.houses).map((house) => {
            return (house.people).map((person) => {
                return (person.name)}).filter((name) => {
                    let nameSurname = name.split(' ');
                    return nameSurname[1].startsWith('S');
                })
                    
        }).flat();
    
        return surnamesWithS;
    } else {
        return ;
    }    
}

module.exports = surnameWithS;
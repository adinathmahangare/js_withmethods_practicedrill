const got = require('./data/data');

function peopleByHouses(got) {
    //2. Write a function called `peopleByHouses` which counts the total number of people in different houses in the `got` variable defined in `data.js` file.

    // let count = 0;
    // let peopleCountByHouse = [];

    // for (let house of got.houses) {
    //     count = 0;
    //     for (let person of house.people) {
    //         count++;            
    //     }
    //     peopleCountByHouse.push([house.name, count]);
    // }

    // return peopleCountByHouse;
    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        let peopleCountByHouse = {};

        (got.houses).map((house) => {
            let houseCount = (house.people).reduce((count, person) => {
                return count + 1;
            },0);

            peopleCountByHouse[house.name] = houseCount;
        })

        return peopleCountByHouse;
    } else {
        return ;
    }

    
}

module.exports = peopleByHouses;
const got = require('./data/data');

function countAllPeople(got) {
    //1. Write a function called `countAllPeople` which counts the total number of people in `got` variable defined in `data.js` file.

    // let nameList = [];

    // for (let house of got.houses) {
    //     for (let person of house.people) {
    //         nameList.push(person.name);
    //     }
    // }

    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let peopleCount = (got.houses).reduce((count, house) => {
            return count + house.people.length;
        }, 0)

        return peopleCount;
    } else {
        return ;
    }
}

module.exports = countAllPeople;
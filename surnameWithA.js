const got = require('./data/data');

function surnameWithA(got) {
    //7. Write a function called `surnameWithA` which returns a array of names of all the people in `got` variable whoes surname is starting with `A`(capital a).
    // your code goes here

    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let surnamesWithA = (got.houses).map((house) => {
            return (house.people).map((person) => {
                return (person.name)}).filter((name) => {
                    let nameSurname = name.split(' ');
                    return nameSurname[1].startsWith('A');
                })               
        }).flat();
    
        return surnamesWithA;
    } else {
        return ;
    } 
    
}

module.exports = surnameWithA;
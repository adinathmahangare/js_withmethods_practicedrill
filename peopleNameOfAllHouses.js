const got = require('./data/data');

function peopleNameOfAllHouses(got) {
    //8. Write a function called `peopleNameOfAllHouses` which returns an object with the key of the name of house and value will be all the people in the house in an array.
    // your code goes here

    // for (let house of got.houses) {
    //     for (let person of house.people) {
            // if (!houseAndPeople[house.name]) {
            //     houseAndPeople[house.name] = [person.name];
            // } else {
            //     houseAndPeople[house.name].push(person.name);
            // }
    //     }
    // }

    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let houseAndPeople = {};

        (got.houses).map((house) => {
            (house.people).map((person) => {
                if (!houseAndPeople[house.name]) {
                    houseAndPeople[house.name] = [person.name];
                } else {
                    houseAndPeople[house.name].push(person.name);
                }
            })
        })

        return houseAndPeople;
    } else {
        return ;
    }     
}

module.exports = peopleNameOfAllHouses;
const got  = require('./data/data');

function nameWithA(got) {
    //5. Write a function called `nameWithA` which returns a array of names of all the people in `got` variable whose name includes `a` or `A`.
    // your code goes here
    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let namesWithA = (got.houses).map((house) => 
            (house.people).map((person) => 
                (person.name)).filter((name) => 
                    name.toLowerCase().includes('a')
                )
            )        
        .flat();
    
        return namesWithA;
    } else {
        return ;
    }       
}

module.exports = nameWithA;




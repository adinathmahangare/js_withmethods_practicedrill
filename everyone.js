const got = require('./data/data')

function everyone(got) {
    //3. Write a function called `everyone` which returns a array of names of all the people in `got` variable.
    // your code goes here

    // let nameList = [];

    // for (let house of got.houses) {
    //     for (let person of house.people) {
    //         nameList.push(person.name);
    //     }
    // }

    // return nameList;

    if (typeof(got) == 'object' && Array.isArray(got.houses)) {
        
        let name = (got.houses).map((house)=> (house.people).map((person) => person.name));

        return name.flat();
    } else {
        return ;
    }   
}

module.exports = everyone;